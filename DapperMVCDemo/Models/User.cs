﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DapperMVCDemo.Models
{
    public class User
    {
        public int ID { set; get; }
        public string Name { set; get; }
        public string Password { set; get; }
        public DateTime AddTime { set; get; }
        public bool Status { set; get; }

    }
}