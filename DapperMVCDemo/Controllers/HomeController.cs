﻿using Dapper;
using DapperMVCDemo.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DapperMVCDemo.Controllers
{
    public class HomeController : ValidateController
    {
        List<User> customers;

        string conn = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;

        public ActionResult Index()
        {
         
            string sql = "select * from [User]";
            using(var connection =  new SqlConnection(conn))
            {
                customers = connection.Query<User>(sql).ToList();
            }
                      

            return View();
        }

        public ActionResult Regist()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Resist(string name,string pwd1,string pwd2)
        {
            if(string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(pwd2) || string.IsNullOrWhiteSpace(pwd2))
            {
                return Content("内容不能为空!");
            }

            string sql = @"INSERT INTO [Statistics].[dbo].[User]
           ([Name]
           ,[Password]
           ,[AddTime]
           ,[Status])
     VALUES
           (@Name
           , @Password
           , @AddTime
           , @Status)";

            using(var connection = new SqlConnection(conn))
            {
                var res = connection.Execute(sql, new
                {
                    Name = name,
                    Password = pwd1,
                    AddTime = DateTime.Now,
                    Status = true
                });
            }

            return View();
        }


        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string name, string pwd1)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(pwd1))
            {
                return Content("输入不对");
            }

            string sql = "SELECT * FROM [Statistics].[dbo].[User] where name = @name";

            using (var connection = new SqlConnection(conn))
            {
                var user = connection.Query<User>(sql, new { name = name }).FirstOrDefault();
                if (user == null)
                {
                    return Content("密码或者账号不对");
                }
                if (user.Password != pwd1)
                {
                    return Content("密码或者账号不对");
                }

                Session["LoginUser"] = user;
            }
            return RedirectToAction("index", "user");
        }

        public ActionResult LogOff()
        {
            Session["LoginUser"] = null;
            return RedirectToAction("Index", "Home");
        }



        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}