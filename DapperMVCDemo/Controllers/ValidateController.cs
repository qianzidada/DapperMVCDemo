﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DapperMVCDemo.Controllers
{
    public class ValidateController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            //var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

            var user = Session["LoginUser"] as User;
            if (user == null)
            {
                //重定向至登录页面
                filterContext.Result = RedirectToAction("Login", "Home", new { url = Request.RawUrl });
                return;
            }

        }

    }
}